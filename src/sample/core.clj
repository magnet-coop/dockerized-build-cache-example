(ns sample.core)

(defn my-double
  [x]
  (* 2 x))
